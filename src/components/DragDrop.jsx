import React, { useState,useContext } from "react";
import { FileUploader } from "react-drag-drop-files";
import  {FileContext}  from "./FileContext";

const fileTypes = ["docx", "xls", "pdf", "pptx", "ppt", "pptm", "png", "jpeg"];

function DragDrop() {
    const {file,setFile} = useContext(FileContext)

    const handleChange = (file) => {
        const nameWithoutExt=file.name.replace(/\.[0-9a-z]+$/,'')
        if (nameWithoutExt.length>=7 && nameWithoutExt.length<=30){
            setFile(file);
        }
        console.log(file)
        
    };
    return (
      <FileUploader
      handleChange={handleChange}
      name="fileUploader" types={fileTypes}
      maxSize={20}/>
    );
  }
  
  export default DragDrop;