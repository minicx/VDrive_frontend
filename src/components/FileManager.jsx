import {React, useEffect, useState} from "react";
import "../styles/fileManager.css"
import ContextMenu from "./ContextMenu";
import { FileUploader } from "react-drag-drop-files";
import DragDrop from "./DragDrop";
import Modal from "./Modal";
import useInput from "../hooks/useInput";
import useApi from "../hooks/useApi.js"
import { useCookies } from 'react-cookie';
import { ModalContext } from "./FileContext";
import { isVisible } from "@testing-library/user-event/dist/utils";
import { useNavigate } from "react-router-dom";
import { PDFReader } from 'reactjs-pdf-reader';
const fileTypes = ["docx", "xls", "pdf", "pptx", "ppt", "pptm", "png", "jpeg"];

export default function FileManager(){
    const [name,setName] = useState()
    const [currentFile,setCurrentFile]=useState()
    const [isVisible,setIsVisible] = useState(false)
    const [cookies, setCookie] = useCookies();
    const [user,setUser] = useState()
    const [directories,setDirectories]= useState()
    const [files,setFiles]= useState()
    const api=useApi()
    useEffect(()=> {
        (async ()=>{
            setUser(await api.fetchUser())
            setDirectories(await api.getDirectories())
            setFiles(await api.getFiles())
            setCurrentFile(await api.downloadFile(ModalContext.id))
        })()
        
    },[])
    const [createInput,setCreateInput] = useState(false)
    const isAdmin= user!=null ? user.isAdmin : false
    
    /* 
    
    fileSystem=[
        {
            id: number,
            name: string,
            needAccess: boolean,
            files: [   // файлы доступные юзеру в этой папке
                {
                    id: number,
                    name: string,
                    needAccess: boolean,
                    uploadDate: DateTime
                }
            ]
        }
    ]
    */
    

    console.log()

    const fileSystem=(directories!==undefined ? directories: []).map((directory)=>{

        const Dfiles=(files!==undefined ? files: []).filter((file)=>{
            if ( file.rootDirectory==directory.id){
                return true
            } else {
                return false
            }
        })
        const avalibleFiles=Dfiles.filter((file)=>{
            if (file.needAccess){
                if (user.accessGranted){
                    return true
                } else {
                    return false
                }
            } else {
                return true
            }
        })
        const needAccess= avalibleFiles.length==0 ? true : false;
        return {
            id : directory.id,
            name : directory.name,
            needAccess: needAccess,
            files : avalibleFiles
        }
    }).filter((directory)=>{
        if (directory.needAccess){
            if (user.accessGranted){
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    })


    const [file, setFile] = useState(null);
    const handleChange = (file) => {
      setFile(file);
    };
    let navigate = useNavigate(); 

    return(
        <ModalContext.Provider value ={{isVisible,setIsVisible}}>
        <main>
            <div className="fileManager">
            <span className="input-group-btn">
            <div className="create">
            {isAdmin && <img src="/add.png" alt="add" className="plus_btn" onClick={()=>setCreateInput(true)}/>}
              {createInput && <div className="createDirectory">
                <input type="" onChange={(e)=>{setName(e.target.value)}}/>
                <button
                    onClick={ async ()=>{
                        console.log(await api.addDirectory(name.replace(/ /g,'')))
                        setCreateInput(false)
                        setName('')
                        window.location.reload()
                    }
                    }>Создать</button>
                </div>}
            </div>
          </span>
                {fileSystem.map((directory) => {
                    return(
                        <ContextMenu directory = {directory}/>
                    )}
                    )}
            </div>
            <div className="filePreview">
                <h1>Просмотр файла</h1>
                {ModalContext.id!==undefined && currentFile && <PDFReader data={currentFile} />}
            </div>
            <Modal/>
        </main>
        </ModalContext.Provider>
        
    )
}