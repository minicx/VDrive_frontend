import React from "react";
import { Link } from "react-router-dom";
import "../styles/home.css";


export default function Home(){
    return(
        <div className="home" on>
            <h1>Корпоративная база знаний <br/> для сотрудников компании</h1>
        
            <Link to="/login"><button className="btn_login">Войти</button></Link>
        </div>
    )
}