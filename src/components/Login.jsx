import React from "react";
import { useState } from "react";
import useInput from "../hooks/useInput";
import { Link } from "react-router-dom";
import '../styles/login.css'
import { useCookies } from 'react-cookie';
import { useNavigate } from "react-router-dom";
export default function Login (){
    const [error,setError] = useState(false)
    const [cookies, setCookie] = useCookies();
    const input = useInput()
    let navigate = useNavigate(); 
    const handleSubmit = async () =>{
        setError(false)
        const data=new FormData();  
        data.append("mail", input.userValue);  
        data.append("password",input.passwordValue);
        const requestOptions = {
                method: 'POST',
                body: data
        };
        const response = await fetch('/api/login', requestOptions);
        if (response.status==200){
                const token = (await response.json()).token;
                setCookie('_auth', token, { path: '/' });
                navigate('/main')
                // setCokie and redirect to another html
        }else {
                // handle error
                const errMessage=(await response.json()).message
                setError(true)
        }
           
    }


    return(
       <div className="login_page">
            <div className="login">
                <h1>Вход</h1>
                <div className="login_form">
                    <div className="username">
                        <input onChange={input.userOnChange} placeholder="Почта"/>
                    </div>
                    <div className="username">
                        <input onChange={input.passwordOnChange} placeholder="Пароль"/>
                    </div>
                </div>
                <div className="bottom">
                    <button className="btn_login" onClick={handleSubmit}>Войти</button>
                </div>
                {error && <span className="error">*Неправильные данные</span>}
                
            </div>
        </div>
    )
}