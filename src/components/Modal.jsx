import {React, useState, useContext} from "react";
import OutsideClickHandler from "react-outside-click-handler";
import "../styles/modal.css"
import DragDrop from "./DragDrop";
import  {FileContext}  from "./FileContext";
import useApi from "../hooks/useApi.js";
import { ModalContext } from "./FileContext";
import { useNavigate } from "react-router-dom";
export default function Modal(){
    const {isVisible, setIsVisible} = useContext(ModalContext)
    const navigator=useNavigate()
    const {file,setFile} = useContext(FileContext)
    const api=useApi()
    if (isVisible) return(
        
            <div className="modal_window">
                <button type="button" class="btn-close" aria-label="Close" onClick={()=>setIsVisible(false)}></button>
                {/* <h1>Загрузить файл</h1> */}
                <DragDrop />
                <button onClick={async ()=>{
                    await api.uploadFile(file,ModalContext.id)
                    window.location.reload()
                }}>Загрузить на сервер</button>
            </div>
    )
}