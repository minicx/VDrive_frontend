import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";

import { useCookies } from "react-cookie";
import '../styles/header.css';

export default function Header({status}){
    let navigate = useNavigate(); 
    const [cookies,setCookie,removeCookie] = useCookies();
    const exit = () =>{
        removeCookie("_auth");
    }
    return(
        <header>
            <div className="header_inner">
                <div className="header_left">
                        <div className="menu">
                            <div className="menu_line">
                            </div>
                            <div className="menu_line">
                            </div>
                            <div className="menu_line">
                            </div>
                        </div>
                    <img src="/logo.png" alt="logo" />
                </div>
                <div className="header_right">
                    {status ? <span onClick={() =>{console.log(status);navigate("/login")}}>Выход</span> : <span onClick={() =>{navigate("/login")}}>Выход</span>}
                </div>
            </div>
        </header>
    )
}