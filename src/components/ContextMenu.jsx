import React, { useEffect, useState,useContext } from "react";
import OutsideClickHandler from 'react-outside-click-handler';
import useApi from "../hooks/useApi";
import '../styles/contextMenu.css'
import { ModalContext } from "./FileContext";

function ContextMenu({directory}) {
    const {isVisible,setIsVisible} = useContext(ModalContext)
    //setIsVisible(true)
    console.log(isVisible)
    const [user,setUser] = useState()
    const api=useApi()
    useEffect(()=>{
        (async ()=>{
            setUser(await api.fetchUser())
        })()
    },[])
    
    const isAdmin= user!=null ? user.isAdmin : false;

    const [displayContextMenuVisible, setdisplayContextMenuVisible] = useState(false);
    const [id,setId]=useState()
    const [type,setType]=useState()
    const [pos, setPos] = useState({ x: 0, y: 0 });
    const displayContextMenu = (e) => {
      e.preventDefault();
      setId(e.target.getAttribute('id'));
      setType(e.target.getAttribute('type'));
      console.log(type,id)
      setdisplayContextMenuVisible(false);
      if (id!==undefined && type!=undefined){
        const updatedPos = {
          x: e.pageX,
          y: e.pageY,
        };
  
        setPos(updatedPos);
        setdisplayContextMenuVisible(true);
        ModalContext.id = id
        
      }

  };
  
  const hideMenu = (event) => {
    setdisplayContextMenuVisible(false);
  };

  const [selectedOpt, setSelectedOpt] = useState();
  const initMenu = (selectedOpt) => {
    setSelectedOpt(selectedOpt);
  };
  const uploadFile=function (){
    console.log('uploadFile')
    setIsVisible(true);
    hideMenu()
  }
  return (
    
    <div
      onContextMenu={displayContextMenu}
    >
        <OutsideClickHandler onOutsideClick={hideMenu}>
            <div className="catalog">
                <span 
                onContextMenu={displayContextMenu}
                onClick={hideMenu}
                type={'directory'}
                id={directory.id}
                >
                {directory.name}
                </span>
                <ul>{directory.files.map((file) => <li onContextMenu={displayContextMenu} onClick={hideMenu} type={"file"} id={file.id}>{file.name}</li >)}</ul>
            </div>
        
          {(displayContextMenuVisible && type!==undefined && id!==undefined) && (
            <div style={{ top: pos.y, left: pos.x }} className="menu-block">
              {type ==='file' && <div className="elOption" onClick={api.downloadFile(id)}>
                Скачать
              </div>}
              {type ==='directory' && (<div className="elOption" >
                <div onClick={()=>uploadFile()}>
                Загрузить файл в папку
                </div>
                
              </div>)}
                {isAdmin &&
                <div className="admin_com">
                <div className="elOption">
                    Переименовать
                </div>
                <div className="elOption"
                // () =>{
                //   if(type === "file"){
                //   directory.files.filter(()=>)
                // }}}>
                >
                    Удалить
                </div>
              </div>
                }
            </div>
          )}
      </OutsideClickHandler>
    </div>
    
  );
          }

export default ContextMenu;