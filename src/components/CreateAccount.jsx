import {React, useState} from "react";
import useInput from "../hooks/useInput";

import { Link } from "react-router-dom";








export default function CreateAccount(){
    const input = useInput()
    const [value, onChange] = useState(new Date());

    const handleSubmit = () =>{
        console.log(1)
    }

    return(
       <div className="login_page">
            <div className="login">
                <h1>Авторизация</h1>
                <div className="login_form">
                    <div className="username">
                        <span>ФИО:</span><input onChange={input.userOnChange} />
                    </div>
                    <div className="username">
                        <span>пароль:</span><input onChange={input.passwordOnChange} placeholder="Пароль"/>
                    </div>
                    <div className="form-floating" defaultValue={"DEFAULT"}>
                        <select className="form-select" id="floatingSelect" aria-label="Floating label select example">
                            <option value={"DEFAULT"} disabled>Нажмите для выбора</option>
                            <option value="1">Коммерческий</option>
                            <option value="2">Финансовый</option>
                            <option value="3">Маркетинговый</option>
                        </select>
                    </div>
                    
                </div>
                <div className="bottom">
                    <button className="btn_login" onClick={handleSubmit}>создать</button>
                </div>
            </div>

        </div>
    )
}