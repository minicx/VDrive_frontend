import { useState } from "react"

export default function useInput(){

    const [userValue,setUserValue] = useState()
    const [passwordValue,setPasswordValue] = useState()
    const [error,setError] = useState(false)

   
       


    let input = {
        userValue,
        passwordValue,
        userOnChange: (e) => setUserValue(e.target.value),
        passwordOnChange: (e) => setPasswordValue(e.target.value),
        error,
        

    }
    return input
}