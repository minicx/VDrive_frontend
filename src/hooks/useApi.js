import { useState } from "react"
import { useCookies } from 'react-cookie';

export default function useApi(){
    const [cookies, setCookie] = useCookies();
    const fetchUser=async function (){
        const requestOptions = {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${cookies['_auth']}`
            }
        };
        const response = await fetch('/api/users/me', requestOptions);

        const user= response.ok ? (await response.json()): null
        return user
    }
    const checkToken=async function (){
        const data=new FormData(); 
        data.append("token", cookies['_auth']);
        const requestOptions = {
            method: 'POST',
            body: data
        };
        const response = await fetch('/api/login/checkToken', requestOptions);
        if (response.status==200){
            return true
        } else {
            return false
        }
    }
    async function getDirectories(){
        const requestOptions = {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${cookies['_auth']}`
            }
        };
        const response = await fetch('/api/directories', requestOptions);
        if (response.status==200){
            return (await response.json())
        } else {
            return []
        }
    }
    async function getFiles(){

        const requestOptions = {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${cookies['_auth']}`
            }
        };
        const response = await fetch('/api/files', requestOptions);

        if (response.status==200){
            return (await response.json())
        } else {
            return []
        }
    }
    async function uploadFile(file,rootDirectory){
        if (rootDirectory!=undefined){
            console.log(rootDirectory,'rootDirecory')
            const data=new FormData(); 
            data.append("name", file.name);
            data.append("file", file);
            data.append('rootDirectory',rootDirectory);
            data.append('needAccess',false);
            const requestOptions = {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${cookies['_auth']}`
                },
                body: data
            };
            const response = await fetch('/api/files', requestOptions);
        } else {
            return null
        }
        
    }
    async function addDirectory(name){
        const data=new FormData(); 
        data.append("name",name);
        const requestOptions = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${cookies['_auth']}`
            },
            body: data
        };
        const response = await fetch('/api/directories', requestOptions);
        console.log(response.status)
        if (response.status==200){
            return true
        } else {
            return false
        }
    }
    async function downloadFile(id){
        console.log(id,204)
        const data=new FormData(); 
        data.append("id",id);
        const requestOptions = {
            method: 'POST',
            headers: {
                'Authorization': `Bearer ${cookies['_auth']}`
            },
            body: data
        };
        const response = await fetch('/api/file', requestOptions);
        console.log(response.status)
        if (response.status==200){
            return await response.arrayBuffer()
        } else {
            return false
        }

    }
    return {
        fetchUser,
        checkToken,
        getDirectories,
        getFiles,
        uploadFile,
        addDirectory,downloadFile
    }
    
}