import './App.css';
import Header from './components/Header';
import { Routes, Route } from 'react-router-dom'
import Login from './components/Login';
import Home from './components/Home';
import Footer from './components/Footer';
import FileManager from './components/FileManager';
import CreateAccount from './components/CreateAccount';
import {React, useEffect, useState, Context } from 'react';
import {middleware} from './middleware/middleware.js'
import { useCookies, Cookies } from 'react-cookie';
import { useNavigate, useLocation } from "react-router-dom";
import useApi from './hooks/useApi.js';
import {FileContext} from './components/FileContext';





//import moment from 'moment';
//import 'moment/locale/ru';

function App() {
  const [file, setFile] = useState(null);

  

  const [log_status,setLog_status] = useState(false)
  const [cookies,setCookie,removeCookie] = useCookies();
  let location = useLocation();
  let navigate = useNavigate(); 
  const api=useApi()
  useEffect(async ()=> {
   
    if (cookies['_auth']!==undefined){
      
      const result=await api.checkToken()
      console.log(result)
      if (result!=true){
        setLog_status(true)
        removeCookie('_auth');
        navigate('/login');
      }else if(result && location.pathname == '/login'){
        navigate('/main');
      }

    } else {
      navigate('/login')
      setLog_status(false)
    }
    
  },[])

  return (
    <div className="App">
      <FileContext.Provider value={{file,setFile}}>
      <Header status = {log_status}/>
      <Routes>

        <Route path='/login' element={<Login/>}></Route>

        <Route path='/main' element={<FileManager/>}></Route>

        <Route path='/createAccount' element={<CreateAccount/>}/>

        <Route path='/' element={<Home/>}/>
      
      </Routes>
      <Footer/>
      </FileContext.Provider>
    </div>
  );
}

export default App;
